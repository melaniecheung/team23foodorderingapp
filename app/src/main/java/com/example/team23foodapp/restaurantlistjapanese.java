package com.example.team23foodapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class restaurantlistjapanese extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurantlistjapanese);

        TextView textView = findViewById(R.id.km_away);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToRestaurantScreen();
            }
        });
    }

    private void navigateToRestaurantScreen() {
        Intent intent = new Intent(this, takeyasushi.class);
        startActivity(intent);
    }
}