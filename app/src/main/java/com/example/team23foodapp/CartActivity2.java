package com.example.team23foodapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class CartActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart2);
        ImageView imageView = findViewById(R.id.checkoutButton2);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToThanksScreen();
            }
        });
    }

    private void navigateToThanksScreen() {
        Intent intent = new Intent(this, ThanksActivity.class);
        startActivity(intent);
    }
}